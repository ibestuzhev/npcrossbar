# README #

Example project to display real-time status updates of Nova Poshta and Meest Express deliveries in browser.

## How to deploy? ##

The project uses `docker` and `docker-compose` so you must have it installed.

Simple

    docker-compose up

should be enough to run the project. After you can access it with http://localhost:8666

There is also a `Makefile` to add some shortcuts:

1. `make docker` to (re)run container in background
1. `make run` to (re)launch container and open browser (Chrome)

The default set of deliveries is empty. The apps tries to read first list of tracking codes from environment variable.

The variable should be named `DELIVERY_${BACKEND}`. The format is JSON.

For docker deliveries can be passed via `deliveryvars` directory.
For example create the `deliveryvars/DELIVERY_INTERNATIONAL` file with

    {"NP20000000079461NPI": "Oakley", "613510538802": "Hose Holder"}

## Directory structure ##

### dock ###

`Dockerfile` and other files (*in future*) to build docker image

### deliveryvars ###

Files to use with `docker-compose` `env_file`.

### node ###

The folder is mounted into docker image, so `crossbar` launches files directly from it.

> **Warning:** as this folder is accessed by *crossbar* user inside docker container, it may raise some permissions issue.
> Check logs and perhaps grant write access to folder or sub-folder

#### node/.crossbar ####

The folder to store crossbar.io config

#### node/app ####

The backend for crossbar. It connects to crossbar router and provides data via RPC and pub/sub

#### node/web ####

The files that should be served to browser (HTML + JS). Note that some JS libraries are loaded from unpkg.com.

It also uses `babel-standalone` to build `jsx` files on the fly.

## Libraries used ##

* [Crossbar.IO](http://crossbar.io/) to run WS server
* [React](https://facebook.github.io/react/) to render results
* [aiohttp](http://aiohttp.readthedocs.io/en/stable/) to run urlopen under asyncio
* [Docker](https://www.docker.com/) and `docker-compose` to prepare environment

## Python Language Server under Docker ##

For VS Code there is a plugin to support the remote interpreter. The idea is called [Language Server](http://langserver.org/).
The Python implementation is [made by Palantir on GitHup](https://github.com/palantir/python-language-server/).

To install it, you need to modify the `vscode-client/src/extension.ts` to specify the port that is used.
Comment the existing `startLangServer("pyls", ["python"])` and uncomment the `startLangServerTCP(2087, ["python"])`.

There is docker image to run the Language server. As it is unstable it's not run with `docker-compose up`. Use

    docker-compose run --rm --service-ports langserver pyls --tcp --host 0.0.0.0

The server may die with segfaults.

## Who do I talk to? ##

* to the hand :)