#!/usr/bin/env python3

import subprocess
import sys

path = sys.argv[-1]

if path.startswith('/home/igor/workspace/novaposhta/node/'):
    path = path.replace('/home/igor/workspace/novaposhta/node/', '/node/')
    subprocess.run(['docker-compose', 'exec', '-T', 'cross', 'sh', '-c',
                    "PYTHONPATH=/node/app pylint {} {}".format(" ".join(sys.argv[1:-1]), path)],
                   stdout=sys.stdout)
else:
    subprocess.run(['/home/igor/.local/bin/pylint'] + sys.argv[1:])
