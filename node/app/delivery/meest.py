"""Meest Inc. parsing"""
from asyncio import as_completed
from datetime import datetime, timedelta
from xml.etree import ElementTree as ET

from aiohttp import ClientSession

from .base import DeliveryInterface


class MeestDelivery(DeliveryInterface):
    """Implementation of `DeliveryInterface` to parse deliveries from Meest Express"""
    identifier = "meest"

    @staticmethod
    async def fetch(session: ClientSession, code: str) -> (str, str):
        """Downloads an URL with aiohttp, returns tuple (tracking_code, xml)"""
        url = "https://t.meest-group.com/get.php?what=tracking&number={0}&ext_track=".format(code)
        async with session.get(url) as resp:
            return code, await resp.text()

    async def update(self, publish: bool = False):
        async with ClientSession() as session:
            tasks = [self.fetch(session, code) for code in self.packages]
            return_data = []
            for loaded_xml in as_completed(tasks):
                code, loaded_xml = await loaded_xml

                parsed_xml = ET.fromstring(loaded_xml)

                last_result = parsed_xml.find('result_table/items[last()]')
                if last_result is None:
                    return_data.append(dict(number=code, info=self.packages[code], status="Not Found"))
                    continue
                result_dict = {a.tag: a.text for a in last_result}
                result_dict.update(number=result_dict['ShipmentNumberSender'],
                                   info=self.packages[result_dict['ShipmentNumberSender']],
                                   status=result_dict['ActionMessages'])
                return_data.append(result_dict)
        return_data.sort(key=lambda i: i['info'])
        self.data = return_data
        self.fetch_time = datetime.now().replace(microsecond=0) + timedelta(hours=3)  # Hack for TZ
        if publish:
            self.publish()
