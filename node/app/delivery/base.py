"""Base class to parse delivery info on asyncio"""

import abc
import typing
import os
import json

import autobahn.wamp.types


class DeliveryInterface(metaclass=abc.ABCMeta):
    """Abstract class that is used to parse and publish information about deliveries.
    """
    DEFAULT_PACKAGES = {}

    packages = None

    @abc.abstractproperty
    def identifier(self) -> str:
        """String used to build remote procedure and pub/sub channel names"""
        pass

    @abc.abstractmethod
    async def update(self, publish: bool = False):
        """Fetch data over API or HTTP and parse it"""
        pass

    def __init__(self, publisher: typing.Callable, details: dict = None):
        self.session_details = details
        self.publisher = publisher

        self.data = []
        self.fetch_time = None
        self.load_packages()

    def load_packages(self):
        """Get list of packages from name or from code"""
        env_name = "DELIVERY_{}".format(self.identifier.upper())
        try:
            self.packages = json.loads(os.environ[env_name])
        except (ValueError, KeyError):
            self.packages = dict(self.DEFAULT_PACKAGES)

    def publish(self, options: autobahn.wamp.types.PublishOptions=None):
        """Publish the data to `self.publisher`"""
        channel = "{}_data".format(self.identifier)
        self.publisher(channel, self.data,
                       fetch_time=str(self.fetch_time or 0),
                       options=options)
