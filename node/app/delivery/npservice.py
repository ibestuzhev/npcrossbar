"""Implementations for Nova Poshta"""
import json
import os
from asyncio import as_completed
from datetime import datetime, timedelta
from html.parser import HTMLParser

from aiohttp import ClientSession

from .base import DeliveryInterface


class NovaPoshtaParser(HTMLParser):
    """Parse the international tracking information, not availabe over API"""
    parsed_row = []
    in_table = False
    in_cell = False

    def reset(self):
        self.parsed_row = []
        super().reset()

    def handle_starttag(self, tag: str, attrs: dict) -> None:
        attrs = dict(attrs)
        if tag == 'table' and 'class' in attrs and 'tracking-int' in attrs['class']:
            self.in_table = True
        elif self.in_table and tag == 'td' and 'class' in attrs \
                and 'char' in attrs['class']:
            self.in_cell = True

    def handle_endtag(self, tag: str):
        if tag == 'table':
            self.in_table = False
        elif tag == 'td':
            self.in_cell = False

    def handle_data(self, data: str):
        if self.in_cell and self.in_table:
            self.parsed_row.append(data)

    def error(self, message):
        print("Shit happend", message)


class DomesticNPDelivery(DeliveryInterface):
    """Implementation of `DeliveryInterface` to parse domestic deliveries from NovaPoshta"""
    API_KEY = os.environ.get("NP_API_KEY", "93532d26d895c23c68e4262217a4b457")
    API_ENDPOINT = "https://api.novaposhta.ua/v2.0/json/"

    identifier = 'domestic'

    async def update(self, publish: bool = False):

        phone = os.environ.get("NP_PHONE", "380991112233")

        data = {
            "apiKey": self.API_KEY,
            "modelName": "TrackingDocument",
            "calledMethod": "getStatusDocuments",
            "methodProperties": {
                "Documents": [{"DocumentNumber": package, "Phone": phone}
                              for package in self.packages.keys()]
            }
        }
        async with ClientSession() as session:
            async with session.post(self.API_ENDPOINT, data=json.dumps(data)) as resp:
                response = await resp.json()
        response = [dict(number=item['Number'],
                         info=self.packages.get(item['Number'], item['Number']),
                         status=item['Status'])
                    for item in response['data']]
        response.sort(key=lambda i: i['info'])
        self.data = response
        self.fetch_time = datetime.now().replace(microsecond=0) + timedelta(hours=3)  # Hack for TZ

        if publish:
            self.publish()


class InternationalNPDelivery(DeliveryInterface):
    """Implementation of `DeliveryInterface` to parse international deliveries from NovaPoshta"""
    identifier = "international"

    async def update(self, publish: bool = False):
        return_data = []
        async with ClientSession() as session:
            tasks = [self.parse_international(c, session) for c in self.packages]
            for data in as_completed(tasks):
                return_data.append(await data)
        return_data.sort(key=lambda i: i['info'])
        self.data = return_data
        self.fetch_time = datetime.now().replace(microsecond=0) + timedelta(hours=3)  # Hack for TZ
        if publish:
            self.publish()

    async def parse_international(self, code: str, session: ClientSession) -> dict:
        """Download the data about international tracking and parse the HTML"""
        url = "https://novaposhta.ua/tracking/international/cargo_number/{}".format(code)
        async with session.get(url) as response:
            data = await response.text()
        parser = NovaPoshtaParser()
        parser.feed(data)
        parser.close()
        return dict(zip(['date', 'status', 'country'], parser.parsed_row),
                    number=code, info=self.packages.get(code, code))
