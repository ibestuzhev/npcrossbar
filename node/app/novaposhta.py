"""NovaPoshta Crossbar backend"""
import os
from asyncio import ensure_future, gather, sleep
from functools import partial

from autobahn.asyncio.wamp import ApplicationRunner, ApplicationSession
from autobahn.wamp.exception import ApplicationError
from autobahn.wamp.types import CallDetails, PublishOptions, RegisterOptions
import envdir

from delivery import DomesticNPDelivery, InternationalNPDelivery, MeestDelivery


class AppSession(ApplicationSession):
    """
    Service that downloads data over NovaPoshta API or Crawling and publishes it over WAMP
    """
    secret = "secret!!!"
    # loc_data = []
    # int_data = []
    # meest_data = []
    # loc_time = int_time = meest_time = None
    periodic = None
    delivery_services = {}
    delivery_classes = (DomesticNPDelivery, InternationalNPDelivery, MeestDelivery)

    def onConnect(self):
        """Run the auth process using ticket"""
        self.join(self.config.realm, ["ticket"], "NovaPoshta")

    def onChallenge(self, challenge):
        """Reply with ticket to finish authentication procedure"""
        if challenge.method == "ticket":
            print("WAMP-Ticket challenge received: {}".format(challenge))
            return self.secret
        else:
            raise Exception("Invalid authmethod {}".format(challenge.method))

    async def onJoin(self, details: dict):
        """Join of this service provider to Crossbar.io router"""
        self.delivery_services = {P.identifier: P(self.publish, details)
                                  for P in self.delivery_classes}

        res = (await self.register(self))
        try:
            # Register a func here as it needs details arg
            await self.register(self.request_publish, 'np.request_publish',
                                RegisterOptions(details_arg='details'))

            for service in self.delivery_services:
                await self.register(partial(self.update, service=service),
                                    'np.updater.{}'.format(service))

        except ApplicationError as reg_error:
            print("REG failed", reg_error)
        if not any(isinstance(x, ApplicationError) for x in res):
            print("Bulk Registration OK")
        else:
            _ = [print(x) for x in res if isinstance(x, ApplicationError)]

        # Wrap in ensure to create a task, so it's executed
        # even if we don't wait for result
        self.periodic = ensure_future(self.periodic_reload())

    async def periodic_reload(self):
        """Periodic task to run the parsers and publish the results"""
        while True:
            await gather(*[self.update(s) for s in self.delivery_services])
            self.request_publish()

            await sleep(3600)

    # Need to do registration without decor to pass an option (fixed in newer version)
    # @register('np.request_publish')
    def request_publish(self, service: str = 'all',
                        details: CallDetails = None):
        """Remote procedure to request a publish.

        Can be run by client on connect

        If caller session id is disclosed in ``details`` it will publish only to that session.
        If details are not available it will publish to all.

        If `service` is set to `"all"` it will publish all deliveries data. In other case
        it should be `DeliveryInterface.identifier`.
        """
        options = None
        if details is not None and details.caller:
            options = PublishOptions(eligible=details.caller)

        if service == 'all':
            publish_services = self.delivery_services.values()
        else:
            publish_services = [self.delivery_services[service]]

        for service_instance in publish_services:
            service_instance.publish(options)

    async def update(self, service: str, publish: bool = False):
        """Calls `DeliveryInterface.update` for service with `service` identifier"""
        await self.delivery_services[service].update(publish)


def main():
    """Main runner"""
    runner = ApplicationRunner(
        "rs://127.0.0.1:8081",
        "realm1",
    )
    runner.run(AppSession)


if __name__ == '__main__':
    if 'ENV_CONFIG_DIR' in os.environ:
        envdir.open(os.environ['ENV_CONFIG_DIR'])
    main()
