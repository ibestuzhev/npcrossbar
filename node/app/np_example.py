#!/usr/bin/env python3
"""Check some packages from Nova Poshta (local only)"""
import json
from urllib.request import urlopen
from html.parser import HTMLParser
from subprocess import Popen
from collections import OrderedDict


API_KEY = "43aeb394172d36999bebb7932f26997c"
API_ENDPOINT = "https://api.novaposhta.ua/v2.0/json/ "
PACKAGES = {
    # '20600003241847': 'Hand Case',
    # '59000251761712': 'Hand Case local',
    # '20600003231893': 'Mirror holder',
    # '59000251768823': 'Mirror local',
    # 'NP20000000079461NPI': 'Oakley',
    # '20600003375321': 'Sticky Pad',
    # '20600003378152': 'Ventilation Holder',
    # '59000252536596': 'Ventilation Holder',
    '20600003394468': 'USAMS',
    # 'NP00000000079251NPI': 'Zenni'
}
INT_PACKAGES = {
    'NP20000000079461NPI': 'Oakley',
    'AE17030000135609NPI': 'USAMS'
}
INT_PACKAGES = OrderedDict([(k, INT_PACKAGES[k]) for k in sorted(INT_PACKAGES)])


class NovaPoshtaParser(HTMLParser):
    """Parse the international tracking information, not availabe over API"""
    parsed_row = []
    in_table = False
    in_cell = False

    def reset(self):
        self.parsed_row = []
        super().reset()

    def handle_starttag(self, tag: str, attrs: dict) -> None:
        attrs = dict(attrs)
        if tag == 'table' and 'class' in attrs and 'tracking-int' in attrs['class']:
            self.in_table = True
        elif self.in_table and tag == 'td' and 'class' in attrs \
                and 'char' in attrs['class']:
            self.in_cell = True

    def handle_endtag(self, tag: str):
        if tag == 'table':
            self.in_table = False
        elif tag == 'td':
            self.in_cell = False

    def handle_data(self, data: str):
        if self.in_cell and self.in_table:
            self.parsed_row.append(data)

    def error(self, message):
        print("Shit happends", message)


def print_info(data: dict):
    """Format the output"""
    for item in data['data']:
        number = item['Number']
        info = PACKAGES.get(number, number)
        status = item['Status']
        print('{}\n * {}\n * {}\n----'.format(info, number, status))


def process_int():
    """Parse the international packages pages"""
    for code, name in INT_PACKAGES.items():
        url = "https://novaposhta.ua/tracking/international/cargo_number/{}".format(code)
        data = urlopen(url).read().decode()
        parser = NovaPoshtaParser()
        parser.feed(data)
        parser.close()
        print(name)
        print(' *', code)
        print(" * {1} ({2})".format(*parser.parsed_row))
        print(" * {0}".format(*parser.parsed_row))
        print('----')

        if parser.parsed_row[1] != 'В сортувальному центрі':
            Popen(['notify-send', 'Kurwa', 'It moved!!!'])


def main():
    """Do the API call"""
    data = {
        "apiKey": API_KEY,
        "modelName": "TrackingDocument",
        "calledMethod": "getStatusDocuments",
        "methodProperties": {
            "Documents": [{"DocumentNumber": package, "Phone": "380979240025"}
                          for package in PACKAGES.keys()]
        }
    }
    req = urlopen(API_ENDPOINT, json.dumps(data).encode())
    response = json.loads(req.read().decode())
    print_info(response)

    process_int()


if __name__ == '__main__':
    main()
