"""Tests for meest parser"""

import unittest
from unittest.mock import patch, MagicMock
from asyncio import get_event_loop, coroutine

from delivery.meest import MeestDelivery


EXAMPLE_RESPONE = """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<return>
<api>API-1C</api>
<apiversion>1</apiversion>
<result_table>
<items>
<ShipmentIdRef>0x80fd1c98ec13526311e728ca17f8c55d</ShipmentIdRef>
<DocumentIdRef>0x80fd1c98ec13526311e728aed0baa240</DocumentIdRef>
<ShipmentNumber>{code}</ShipmentNumber>
<ShipmentNumberSender>{code}</ShipmentNumberSender>
<ShipmentNumberTransit></ShipmentNumberTransit>
<Barcode>CV576582959EE</Barcode>
<DateTimeAction>2017-04-24 08:27:18</DateTimeAction>
<Country>КИТАЙ </Country>
<Country_RU>КИТАЙ</Country_RU>
<Country_EN>CHINA</Country_EN>
<City>Львів</City>
<City_RU>Львов</City_RU>
<City_EN>Lviv (LVSK) (UA)</City_EN>
<StatusCode>101</StatusCode>
<ActionId>1</ActionId>
<AgentsIdRef>0xa7f1001ec93df11f11e6a4eb4286be74</AgentsIdRef>
<AgentCode>MG-007067</AgentCode>
<ActionMessages>Оформлено для відправки</ActionMessages>
<DetailMessages></DetailMessages>
<ActionMessages_RU>Оформлено для отправки</ActionMessages_RU>
<DetailMessages_RU></DetailMessages_RU>
<ActionMessages_EN>POSTING / COLLECTION</ActionMessages_EN>
<DetailMessages_EN></DetailMessages_EN>
<DetailPlacesAction>1/1</DetailPlacesAction>
</items>
<items>
<ShipmentIdRef>0x80fd1c98ec13526311e728ca17f8c55d</ShipmentIdRef>
<DocumentIdRef>0x80fe1c98ec13526311e72c34e7bfd34f</DocumentIdRef>
<ShipmentNumber>{code}</ShipmentNumber>
<ShipmentNumberSender>{code}</ShipmentNumberSender>
<ShipmentNumberTransit></ShipmentNumberTransit>
<Barcode>CV576582959EE</Barcode>
<DateTimeAction>2017-04-28 08:59:17</DateTimeAction>
<Country>УКРАЇНА</Country>
<Country_RU>УКРАИНА</Country_RU>
<Country_EN>UKRAINE</Country_EN>
<City>Львів</City>
<City_RU>Львів</City_RU>
<City_EN>Lviv</City_EN>
<StatusCode>505</StatusCode>
<ActionId>5</ActionId>
<AgentsIdRef>0xa7f1001ec93df11f11e6a4eb4286be74</AgentsIdRef>
<AgentCode>MG-007067</AgentCode>
<ActionMessages>Поступлення посилки на Місце Міжнародного поштового обміну з</ActionMessages>
<DetailMessages>КИТАЙ </DetailMessages>
<ActionMessages_RU>Поступление отправления на Место Международного почтового обмена с </ActionMessages_RU>
<DetailMessages_RU>КИТАЙ</DetailMessages_RU>
<ActionMessages_EN>Receipt of Parcel from</ActionMessages_EN>
<DetailMessages_EN>CHINA </DetailMessages_EN>
<DetailPlacesAction>1/1</DetailPlacesAction>
</items>
<items>
<ShipmentIdRef>0x80fd1c98ec13526311e728ca17f8c55d</ShipmentIdRef>
<DocumentIdRef>0x80fe1c98ec13526311e72bd4962f6153</DocumentIdRef>
<ShipmentNumber>{code}</ShipmentNumber>
<ShipmentNumberSender>{code}</ShipmentNumberSender>
<ShipmentNumberTransit></ShipmentNumberTransit>
<Barcode>CV576582959EE</Barcode>
<DateTimeAction>2017-04-28 21:41:31</DateTimeAction>
<Country>УКРАЇНА</Country>
<Country_RU>УКРАИНА</Country_RU>
<Country_EN>UKRAINE</Country_EN>
<City>Львів</City>
<City_RU>Львів</City_RU>
<City_EN>Lviv</City_EN>
<StatusCode>2028</StatusCode>
<ActionId>20</ActionId>
<AgentsIdRef>0xa7f1001ec93df11f11e6a4eb4286be74</AgentsIdRef>
<AgentCode>MG-007067</AgentCode>
<ActionMessages>Посилка відвантажена у напрямку</ActionMessages>
<DetailMessages>Дніпро</DetailMessages>
<ActionMessages_RU>Отправлено в направлении</ActionMessages_RU>
<DetailMessages_RU>Дніпро</DetailMessages_RU>
<ActionMessages_EN>Parcel been sent to the sorting center</ActionMessages_EN>
<DetailMessages_EN>Dnipro</DetailMessages_EN>
<DetailPlacesAction>1/1</DetailPlacesAction>
</items>
<items>
<ShipmentIdRef>0x80fd1c98ec13526311e728ca17f8c55d</ShipmentIdRef>
<DocumentIdRef>0x80fe1c98ec13526311e72e5a97d98d6f</DocumentIdRef>
<ShipmentNumber>{code}</ShipmentNumber>
<ShipmentNumberSender>{code}</ShipmentNumberSender>
<ShipmentNumberTransit></ShipmentNumberTransit>
<Barcode>CV576582959EE</Barcode>
<DateTimeAction>2017-05-01 13:40:25</DateTimeAction>
<Country>УКРАЇНА</Country>
<Country_RU>УКРАИНА</Country_RU>
<Country_EN>UKRAINE</Country_EN>
<City>Дніпро (Дніпропетровськ)</City>
<City_RU>Дніпро (Дніпропетровськ)</City_RU>
<City_EN>Dnipro (Dnipropetrovs'k)</City_EN>
<StatusCode>2129</StatusCode>
<ActionId>21</ActionId>
<AgentsIdRef>0xa7f1001ec93df11f11e6a4eb4286be74</AgentsIdRef>
<AgentCode>MG-007067</AgentCode>
<ActionMessages>Посилку прийнято підрозділом</ActionMessages>
<DetailMessages>Дніпро</DetailMessages>
<ActionMessages_RU>Посылка принята подразделением</ActionMessages_RU>
<DetailMessages_RU>Дніпро</DetailMessages_RU>
<ActionMessages_EN>Parcel been receipt at branch</ActionMessages_EN>
<DetailMessages_EN>Dnipro</DetailMessages_EN>
<DetailPlacesAction>1/1</DetailPlacesAction>
</items>
<items>
<ShipmentIdRef>0x80fd1c98ec13526311e728ca17f8c55d</ShipmentIdRef>
<DocumentIdRef>0x80fe1c98ec13526311e72fc034e2c9ce</DocumentIdRef>
<ShipmentNumber>{code}</ShipmentNumber>
<ShipmentNumberSender>{code}</ShipmentNumberSender>
<ShipmentNumberTransit></ShipmentNumberTransit>
<Barcode>CV576582959EE</Barcode>
<DateTimeAction>2017-05-03 08:37:13</DateTimeAction>
<Country>УКРАЇНА</Country>
<Country_RU>УКРАИНА</Country_RU>
<Country_EN>UKRAINE</Country_EN>
<City>Дніпро (Дніпропетровськ)</City>
<City_RU>Дніпро (Дніпропетровськ)</City_RU>
<City_EN>Dnipro (Dnipropetrovs'k)</City_EN>
<StatusCode>1315</StatusCode>
<ActionId>13</ActionId>
<AgentsIdRef>0xa7f1001ec93df11f11e6a4eb4286be74</AgentsIdRef>
<AgentCode>MG-007067</AgentCode>
<ActionMessages>Відправлення видано кур`єру на доручення</ActionMessages>
<DetailMessages></DetailMessages>
<ActionMessages_RU>Отправление выдано курьеру на доставку</ActionMessages_RU>
<DetailMessages_RU></DetailMessages_RU>
<ActionMessages_EN>Handed over to the courier</ActionMessages_EN>
<DetailMessages_EN></DetailMessages_EN>
<DetailPlacesAction>1/1</DetailPlacesAction>
</items>
<items>
<DateTimeAction>2017-05-03 14:11:16</DateTimeAction>
<Country>УКРАЇНА</Country>
<City>Дніпро (Дніпропетровськ)</City>
<DetailPlacesAction>1/1</DetailPlacesAction>
<ActionMessages>Кур`єром заплановано час доручення 			з 14:00 по 15:00</ActionMessages>
<DetailMessages></DetailMessages>
<ActionMessages_RU>Курьером запланировано время доставки c 14:00 по 15:00</ActionMessages_RU>
<DetailMessages_RU></DetailMessages_RU>
<ActionMessages_EN>Delivery time planned by the courier is from  14:00 till 15:00</ActionMessages_EN>
<DetailMessages_EN></DetailMessages_EN>
<ActionId>160</ActionId>
<ShipmentIdRef>0x80fd1c98ec13526311e728ca17f8c55d</ShipmentIdRef>
<DocumentIdRef>0x80fd1c98ec13526311e728aed0baa240</DocumentIdRef>
<ShipmentNumber>{code}</ShipmentNumber>
<ShipmentNumberSender>{code}</ShipmentNumberSender>
<ShipmentNumberTransit></ShipmentNumberTransit>
<Barcode>CV576582959EE</Barcode>
<AgentsIdRef>0xa7f1001ec93df11f11e6a4eb4286be74</AgentsIdRef>
<AgentCode>MG-007067</AgentCode>
<Country_RU>УКРАИНА</Country_RU>
<Country_EN>UKRAINE</Country_EN>
<City_RU>Дніпро (Дніпропетровськ)</City_RU>
<City_EN>Dnipro (Dnipropetrovs'k)</City_EN>
</items>
<items>
<DateTimeAction>2017-05-03 14:52:27</DateTimeAction>
<Country>УКРАЇНА</Country>
<City>Дніпро (Дніпропетровськ)</City>
<DetailPlacesAction>1/1</DetailPlacesAction>
<ActionMessages>Попередні дані з маршруту доставки: Відправлення доставлено отримувачу</ActionMessages>
<DetailMessages></DetailMessages>
<ActionMessages_RU>Предыдущие данные с маршрута доставки: Отправление вручено получателю</ActionMessages_RU>
<DetailMessages_RU></DetailMessages_RU>
<ActionMessages_EN>Preliminary data from the route of delivery: Delivered to the recipient</ActionMessages_EN>
<DetailMessages_EN></DetailMessages_EN>
<ActionId>160</ActionId>
<ShipmentIdRef>0x80fd1c98ec13526311e728ca17f8c55d</ShipmentIdRef>
<DocumentIdRef>0x80fd1c98ec13526311e728aed0baa240</DocumentIdRef>
<ShipmentNumber>{code}</ShipmentNumber>
<ShipmentNumberSender>{code}</ShipmentNumberSender>
<ShipmentNumberTransit></ShipmentNumberTransit>
<Barcode>CV576582959EE</Barcode>
<AgentsIdRef>0xa7f1001ec93df11f11e6a4eb4286be74</AgentsIdRef>
<AgentCode>MG-007067</AgentCode>
<Country_RU>УКРАИНА</Country_RU>
<Country_EN>UKRAINE</Country_EN>
<City_RU>Дніпро (Дніпропетровськ)</City_RU>
<City_EN>Dnipro (Dnipropetrovs'k)</City_EN>
</items>
<items>
<ShipmentIdRef>0x80fd1c98ec13526311e728ca17f8c55d</ShipmentIdRef>
<DocumentIdRef>0x80fe1c98ec13526311e73006e3a2b586</DocumentIdRef>
<ShipmentNumber>{code}</ShipmentNumber>
<ShipmentNumberSender>{code}</ShipmentNumberSender>
<ShipmentNumberTransit></ShipmentNumberTransit>
<Barcode>CV576582959EE</Barcode>
<DateTimeAction>2017-05-03 16:46:30</DateTimeAction>
<Country>УКРАЇНА</Country>
<Country_RU>УКРАИНА</Country_RU>
<Country_EN>UKRAINE</Country_EN>
<City>Дніпро (Дніпропетровськ)</City>
<City_RU>Дніпро (Дніпропетровськ)</City_RU>
<City_EN>Dnipro (Dnipropetrovs'k)</City_EN>
<StatusCode>1622</StatusCode>
<ActionId>16</ActionId>
<AgentsIdRef>0xa7f1001ec93df11f11e6a4eb4286be74</AgentsIdRef>
<AgentCode>MG-007067</AgentCode>
<ActionMessages>Доставлено</ActionMessages>
<DetailMessages>03.05.2017</DetailMessages>
<ActionMessages_RU>Доставлено</ActionMessages_RU>
<DetailMessages_RU>03.05.2017</DetailMessages_RU>
<ActionMessages_EN>Delivered</ActionMessages_EN>
<DetailMessages_EN>03.05.2017</DetailMessages_EN>
<DetailPlacesAction>1/1</DetailPlacesAction>
</items>
</result_table>
<result_uuid></result_uuid>
<errors>
<code>000</code>
<name> </name>
</errors>
</return>"""


class MagicResponse(MagicMock):
    """Mock the response from aiohttp"""
    async def __aenter__(self):
        return self

    async def __aexit__(self, *args):
        pass

    @coroutine
    def text(self):
        """Return the response from HTTP Request"""
        return EXAMPLE_RESPONE.format(code=self.url[55:-11])


class MockedSession(MagicMock):
    """Mock for aiohttp client"""
    async def __aenter__(self):
        return self

    async def __aexit__(self, *args):
        pass

    def get(self, url):
        """HTTP GET request"""
        return MagicResponse(url=url)


@patch('delivery.meest.ClientSession', new_callable=MockedSession)
class TestMeestUpdate(unittest.TestCase):
    """Tests for meest parser"""

    def setUp(self):
        self.publisher = MagicMock()
        self.delivery_backend = MeestDelivery(self.publisher)
        self.delivery_backend.packages = {'x00000001': "Test 1", 'x0000002': "Test 2"}

    def test_update(self, http_mock: MockedSession):
        """Check that response is parsed into dict"""
        self.assertIsNotNone(self.delivery_backend)
        coro = self.delivery_backend.update(False)
        get_event_loop().run_until_complete(coro)

        self.assertEqual(len(self.delivery_backend.data), 2)

        self.assertEqual(self.delivery_backend.data[0]['status'], 'Доставлено')

        self.publisher.assert_not_called()

    def test_update_publish(self, http_mock: MockedSession):
        """Test publishing of response after update"""
        coro = self.delivery_backend.update(True)
        get_event_loop().run_until_complete(coro)
        self.assertEqual(self.publisher.call_count, 1)
        call_args, call_kwargs = self.publisher.call_args
        self.assertEqual(call_args[0], 'meest_data')
        self.assertIn('fetch_time', call_kwargs)
        self.assertIsNone(call_kwargs['options'])

if __name__ == '__main__':
    unittest.main()
