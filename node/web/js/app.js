'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _templateObject = _taggedTemplateLiteral(['https://novaposhta.ua/tracking/?cargo_number=', ''], ['https://novaposhta.ua/tracking/?cargo_number=', '']),
    _templateObject2 = _taggedTemplateLiteral(['https://t.meest-group.com/', ''], ['https://t.meest-group.com/', '']),
    _templateObject3 = _taggedTemplateLiteral(['https://novaposhta.ua/tracking/international/cargo_number/', ''], ['https://novaposhta.ua/tracking/international/cargo_number/', '']);

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function template(strings) {
    for (var _len = arguments.length, keys = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        keys[_key - 1] = arguments[_key];
    }

    return function () {
        for (var _len2 = arguments.length, values = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
            values[_key2] = arguments[_key2];
        }

        var dict = values[values.length - 1] || {};
        var result = [strings[0]];
        keys.forEach(function (key, i) {
            var value = Number.isInteger(key) ? values[key] : dict[key];
            result.push(value, strings[i + 1]);
        });
        return result.join('');
    };
}

var NPTable = function (_React$Component) {
    _inherits(NPTable, _React$Component);

    function NPTable() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, NPTable);

        for (var _len3 = arguments.length, args = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
            args[_key3] = arguments[_key3];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = NPTable.__proto__ || Object.getPrototypeOf(NPTable)).call.apply(_ref, [this].concat(args))), _this), _this.urlTemplate = template(_templateObject, 'code'), _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(NPTable, [{
        key: 'render',

        // prefix = 'domestic';

        value: function render() {
            var _this2 = this;

            return React.createElement(
                'table',
                { className: 'table' },
                React.createElement(
                    'thead',
                    null,
                    React.createElement(
                        'tr',
                        null,
                        React.createElement(
                            'th',
                            null,
                            'Name'
                        ),
                        React.createElement(
                            'th',
                            null,
                            'Number'
                        ),
                        React.createElement(
                            'th',
                            null,
                            'Status'
                        )
                    )
                ),
                React.createElement(
                    'tbody',
                    null,
                    this.props.data.map(function (item) {
                        return React.createElement(
                            'tr',
                            { key: _this2.prefix + item.number },
                            React.createElement(
                                'td',
                                null,
                                item.info
                            ),
                            React.createElement(
                                'td',
                                null,
                                React.createElement(
                                    'a',
                                    { href: _this2.doUrl(item.number), target: '_blank' },
                                    item.number
                                )
                            ),
                            React.createElement(
                                'td',
                                null,
                                _this2.getStatus(item)
                            )
                        );
                    })
                ),
                React.createElement(
                    'tfoot',
                    null,
                    React.createElement(
                        'tr',
                        null,
                        React.createElement(
                            'td',
                            { colSpan: '3', className: 'text-right' },
                            React.createElement(
                                'em',
                                null,
                                'Last update at ',
                                '' + this.props.lastUpdate,
                                React.createElement('br', null),
                                'Last fetch on server at ',
                                this.props.lastServerUpdate
                            ),
                            '\xA0',
                            React.createElement(
                                'button',
                                { className: 'btn btn-danger btn-xs', onClick: function onClick() {
                                        _this2.props.forceReloader(_this2.prefix);
                                    } },
                                'force'
                            )
                        )
                    )
                )
            );
        }
    }, {
        key: 'getStatus',
        value: function getStatus(item) {
            if (this.props.statusFormat) {
                return this.props.statusFormat(item);
            }
            return item.status;
        }
    }, {
        key: 'doUrl',
        value: function doUrl(code) {
            var urlTpl = this.props.urlTemplate || this.urlTemplate;
            return urlTpl({ code: code });
        }
    }, {
        key: 'prefix',
        get: function get() {
            return this.props.prefix || 'domestic';
        },
        set: function set(p) {}
    }]);

    return NPTable;
}(React.Component);

var renderUtils = {
    meest: {
        urlTemplate: template(_templateObject2, 'code'),
        getStatus: function getStatus(item) {
            return React.createElement(
                'span',
                null,
                item.status,
                ' ',
                item.DetailMessages && '(' + item.DetailMessages + ')',
                React.createElement('br', null),
                'at ',
                item.City,
                ', ',
                item.Country,
                React.createElement('br', null),
                item.DateTimeAction
            );
        }
    },
    international: {
        urlTemplate: template(_templateObject3, 'code'),
        getStatus: function getStatus(item) {
            return React.createElement(
                'span',
                null,
                item.status,
                ' ',
                item.country && '(' + item.country + ')',
                React.createElement('br', null),
                item.date
            );
        }
    }
};

var NPRoot = function (_React$Component2) {
    _inherits(NPRoot, _React$Component2);

    function NPRoot(props) {
        _classCallCheck(this, NPRoot);

        var _this3 = _possibleConstructorReturn(this, (NPRoot.__proto__ || Object.getPrototypeOf(NPRoot)).call(this, props));

        _this3.state = {
            // loc_data: [{"number":"20600003394468","info":"USAMS","status":"Нова пошта очікує надходження від відправника"}],
            // int_data: [{"date":"13.04.2017 16:00:22","status":"В сортувальному центрі","country":"Німеччина","number":"NP20000000079461NPI","info":"Oakley"},{"date":"18.04.2017 15:00:43","status":"В сортувальному центрі","country":"Україна","number":"AE17030000135609NPI","info":"USAMS"}],
            loc_data: [],
            int_data: [],
            meest_data: [],
            loc_update: new Date(2017, 3, 25),
            int_update: new Date(2017, 3, 25),
            meest_update: new Date(2017, 3, 30)
        };
        return _this3;
    }

    _createClass(NPRoot, [{
        key: 'render',
        value: function render() {
            var _this4 = this;

            var intUrlTemplate = template(_templateObject3, 'code');
            var intGetStatus = function intGetStatus(item) {
                return React.createElement(
                    'span',
                    null,
                    item.status,
                    ' (',
                    item.country,
                    ')',
                    React.createElement('br', null),
                    item.date
                );
            };
            return React.createElement(
                'div',
                null,
                React.createElement(
                    'h2',
                    null,
                    'Local deliveries'
                ),
                React.createElement(
                    'p',
                    null,
                    React.createElement(
                        'button',
                        { onClick: function onClick() {
                                _this4.requestUpdate('domestic');
                            },
                            className: 'btn btn-primary' },
                        'Reload'
                    )
                ),
                React.createElement(NPTable, { data: this.state.loc_data, lastUpdate: this.state.loc_update, lastServerUpdate: this.state.loc_fetch,
                    forceReloader: this.forceReloader.bind(this) }),
                React.createElement(
                    'h2',
                    null,
                    'International deliveries'
                ),
                React.createElement(
                    'p',
                    null,
                    React.createElement(
                        'button',
                        { onClick: function onClick() {
                                _this4.requestUpdate('international');
                            },
                            className: 'btn btn-primary' },
                        'Reload'
                    )
                ),
                React.createElement(NPTable, { data: this.state.int_data, lastUpdate: this.state.int_update,
                    lastServerUpdate: this.state.int_fetch,
                    forceReloader: this.forceReloader.bind(this),
                    prefix: 'international',
                    urlTemplate: renderUtils.international.urlTemplate,
                    statusFormat: renderUtils.international.getStatus }),
                React.createElement(
                    'h1',
                    null,
                    'Meest deliveries (lol)'
                ),
                React.createElement(
                    'p',
                    null,
                    React.createElement(
                        'button',
                        { onClick: function onClick() {
                                _this4.requestUpdate('meest');
                            },
                            className: 'btn btn-primary' },
                        'Reload'
                    )
                ),
                React.createElement(NPTable, { data: this.state.meest_data, lastUpdate: this.state.meest_update,
                    lastServerUpdate: this.state.meest_fetch,
                    forceReloader: this.forceReloader.bind(this),
                    prefix: 'meest',
                    urlTemplate: renderUtils.meest.urlTemplate,
                    statusFormat: renderUtils.meest.getStatus })
            );
        }
    }, {
        key: 'requestUpdate',
        value: function requestUpdate(updType) {
            this.session.call('np.request_publish', [updType]);
        }
    }, {
        key: 'forceReloader',
        value: function forceReloader(prefix) {
            var funcName = 'np.updater.' + (prefix || 'domestic');
            this.session.call(funcName, [], { 'publish': true });
        }
    }, {
        key: 'getUpdateListener',
        value: function getUpdateListener(type) {
            var _this5 = this;

            return function (args, kwargs, details) {
                var data = args[0];
                var newState = {};
                var oldStateMap = new Map();

                _this5.state[type + '_data'].forEach(function (el) {
                    oldStateMap.set(el.number, el.status);
                });

                newState[type + '_data'] = data;
                newState[type + '_update'] = new Date();
                newState[type + '_fetch'] = kwargs.fetch_time || "0";
                _this5.setState(newState);
                if (_this5.showNotification) {
                    var _iteratorNormalCompletion = true;
                    var _didIteratorError = false;
                    var _iteratorError = undefined;

                    try {
                        for (var _iterator = data[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                            var item = _step.value;

                            var oldStatus = oldStateMap.get(item.number);
                            if (typeof oldStatus !== 'undefined' && item.status !== oldStatus) {
                                var options = {
                                    body: 'It moved ' + item.number,
                                    icon: 'https://npshopping.com/favicon.png'
                                };
                                new Notification('KURWA', options);
                            }
                        }
                    } catch (err) {
                        _didIteratorError = true;
                        _iteratorError = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion && _iterator.return) {
                                _iterator.return();
                            }
                        } finally {
                            if (_didIteratorError) {
                                throw _iteratorError;
                            }
                        }
                    }
                }
            };
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this6 = this;

            var wsuri = this.props.ws;
            // the WAMP connection to the Router
            //
            this.connection = new autobahn.Connection({
                url: wsuri,
                realm: "realm1"
            });

            this.connection.onopen = function (ses, details) {
                _this6.session = ses;
                ses.subscribe('domestic_data', _this6.getUpdateListener('loc'));
                ses.subscribe('international_data', _this6.getUpdateListener('int'));
                ses.subscribe('meest_data', _this6.getUpdateListener('meest'));

                // ses.call("np.updater.local", [], {publish: true});
                // ses.call("np.updater.int", [true]);
                ses.call('np.request_publish');
            };
            this.connection.open();
            Notification.requestPermission().then(function (result) {
                _this6.showNotification = result === 'granted';
            });
        }
    }]);

    return NPRoot;
}(React.Component);
//# sourceMappingURL=app.js.map
