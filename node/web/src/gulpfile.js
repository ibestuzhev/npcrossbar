var gulp = require('gulp');
var babel = require("gulp-babel");
var mainBowerFiles = require('main-bower-files');
const sourcemaps = require('gulp-sourcemaps');

gulp.task('copybower', function () {
    return gulp.src(mainBowerFiles())
        .pipe(gulp.dest('../js/'))
})

gulp.task('buildreact', function () {
    return gulp.src('react/app.jsx')
        .pipe(sourcemaps.init())
        .pipe(babel())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('../js/'))
})

gulp.task('default', ["copybower", "buildreact"])