function template(strings, ...keys) {
    return (function (...values) {
        var dict = values[values.length - 1] || {};
        var result = [strings[0]];
        keys.forEach(function (key, i) {
            var value = Number.isInteger(key) ? values[key] : dict[key];
            result.push(value, strings[i + 1]);
        });
        return result.join('');
    });
}

class NPTable extends React.Component {
    urlTemplate = template `https://novaposhta.ua/tracking/?cargo_number=${'code'}`;
    // prefix = 'domestic';

    render() {
        return (
            <table className="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Number</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                    {this.props.data.map(item => (
                        <tr key={this.prefix + item.number}>
                            <td>{item.info}</td>
                            <td>
                                <a href={this.doUrl(item.number)} target="_blank">
                                {item.number}
                                </a>
                            </td>
                            <td>{this.getStatus(item)}</td>
                        </tr>
                    ))}
                </tbody>
                <tfoot>
                    <tr>
                        <td colSpan="3" className="text-right">
                            <em>
                                Last update at {`${this.props.lastUpdate}`}
                                <br/>
                                Last fetch on server at {this.props.lastServerUpdate}
                            </em>
                            &nbsp;
                            <button className="btn btn-danger btn-xs" onClick={() => {this.props.forceReloader(this.prefix)}}>force</button>
                        </td>
                    </tr>
                </tfoot>
            </table>
        )
    }

    get prefix () {
        return this.props.prefix || 'domestic'
    }
    set prefix (p) {}

    getStatus(item) {
        if (this.props.statusFormat) {
            return this.props.statusFormat(item)
        }
        return item.status
    }
    doUrl(code) {
        let urlTpl = this.props.urlTemplate || this.urlTemplate;
        return urlTpl({code});
    }
}

var renderUtils = {
    meest: {
        urlTemplate: (template `https://t.meest-group.com/${'code'}`),
        getStatus: (item) => (
            <span>
                {item.status} {item.DetailMessages && `(${item.DetailMessages})`}
                <br/>
                at {item.City}, {item.Country}
                <br/>
                {item.DateTimeAction}
            </span>
        )
    },
    international: {
        urlTemplate: (template `https://novaposhta.ua/tracking/international/cargo_number/${'code'}`),
        getStatus: (item) => (
            <span>
                {item.status} {item.country && `(${item.country})`}
                <br/>
                {item.date}
            </span>
        )
    }
}

class NPRoot extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            // loc_data: [{"number":"20600003394468","info":"USAMS","status":"Нова пошта очікує надходження від відправника"}],
            // int_data: [{"date":"13.04.2017 16:00:22","status":"В сортувальному центрі","country":"Німеччина","number":"NP20000000079461NPI","info":"Oakley"},{"date":"18.04.2017 15:00:43","status":"В сортувальному центрі","country":"Україна","number":"AE17030000135609NPI","info":"USAMS"}],
            loc_data: [],
            int_data: [],
            meest_data: [],
            loc_update: new Date(2017, 3, 25),
            int_update: new Date(2017, 3, 25),
            meest_update: new Date(2017, 3, 30)
        }
    }
    render() {
        let intUrlTemplate = template `https://novaposhta.ua/tracking/international/cargo_number/${'code'}`;
        let intGetStatus = (item) => (
            <span>
                {item.status} ({item.country})
                <br/>
                {item.date}
            </span>
        )
        return (
            <div>
            <h2>Local deliveries</h2>
            <p>
                <button onClick={() => { this.requestUpdate('domestic') }}
                        className="btn btn-primary">
                    Reload
                </button>
            </p>
            <NPTable data={this.state.loc_data} lastUpdate={this.state.loc_update} lastServerUpdate={this.state.loc_fetch}
                     forceReloader={this.forceReloader.bind(this)} />
            <h2>International deliveries</h2>
            <p>
                <button onClick={() => { this.requestUpdate('international') }}
                        className="btn btn-primary">
                    Reload
                </button>
            </p>
            <NPTable data={this.state.int_data} lastUpdate={this.state.int_update}
                     lastServerUpdate={this.state.int_fetch}
                     forceReloader={this.forceReloader.bind(this)}
                     prefix="international"
                     urlTemplate={renderUtils.international.urlTemplate}
                     statusFormat={renderUtils.international.getStatus} />
            <h1>Meest deliveries (lol)</h1>
            <p>
                <button onClick={() => { this.requestUpdate('meest') }}
                        className="btn btn-primary">
                    Reload
                </button>
            </p>
            <NPTable data={this.state.meest_data} lastUpdate={this.state.meest_update}
                     lastServerUpdate={this.state.meest_fetch}
                     forceReloader={this.forceReloader.bind(this)}
                     prefix="meest"
                     urlTemplate={renderUtils.meest.urlTemplate}
                     statusFormat={renderUtils.meest.getStatus} />
            </div>
        )
    }
    requestUpdate(updType) {
        this.session.call('np.request_publish', [updType]);
    }
    forceReloader(prefix) {
        var funcName = 'np.updater.' + (prefix || 'domestic');
        this.session.call(funcName, [], {'publish': true});
    }
    getUpdateListener(type) {
        return (args, kwargs, details) => {
            let data = args[0];
            let newState = {};
            let oldStateMap = new Map();

            this.state[type + '_data'].forEach((el) => {
                oldStateMap.set(el.number, el.status);
            })

            newState[type + '_data'] = data;
            newState[type + '_update'] = new Date();
            newState[type + '_fetch'] = kwargs.fetch_time || "0";
            this.setState(newState);
            if (this.showNotification) {
                for (let item of data) {
                    let oldStatus = oldStateMap.get(item.number)
                    if (typeof oldStatus !== 'undefined' && item.status !== oldStatus) {
                        let options = {
                            body: 'It moved ' + item.number,
                            icon: 'https://npshopping.com/favicon.png'
                        }
                        new Notification('KURWA', options)
                    }
                }
            }
        }
    }
    componentDidMount() {
        let wsuri = this.props.ws;
        // the WAMP connection to the Router
        //
        this.connection = new autobahn.Connection({
            url: wsuri,
            realm: "realm1"
        });

        this.connection.onopen = (ses, details) => {
            this.session = ses;
            ses.subscribe('domestic_data', this.getUpdateListener('loc'));
            ses.subscribe('international_data', this.getUpdateListener('int'));
            ses.subscribe('meest_data', this.getUpdateListener('meest'));

            // ses.call("np.updater.local", [], {publish: true});
            // ses.call("np.updater.int", [true]);
            ses.call('np.request_publish');
        }
        this.connection.open();
        Notification.requestPermission().then((result) => {
            this.showNotification = (result === 'granted');
        })
    }
}
