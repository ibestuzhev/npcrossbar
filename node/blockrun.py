# import asyncio
from autobahn.twisted.wamp import ApplicationSession, ApplicationRunner
from twisted.internet.defer import inlineCallbacks
from twisted.internet import reactor

class OneRun(ApplicationSession):
    @inlineCallbacks
    def onJoin(self, details):
        yield self.call("np.updater.int", publish=True)
        yield self.call("np.updater.local", publish=True)
        (yield self.leave()).close()
        reactor.stop()


if __name__ == '__main__':
    runner = ApplicationRunner(
        "ws://127.0.0.1:8080/ws",
        "realm1",
    )
    res = runner.run(OneRun)
    print('We are out')