.PHONY: lol docker browser run sleep stop-docker build test

lol:
	echo lol

stop-docker:
	docker-compose stop

build: dock/buildflag
dock/buildflag: docker-compose.yml dock/Dockerfile dock/Dockerjs
	docker-compose build
	touch dock/buildflag

docker: stop-docker build
	docker-compose up -d

browser:
	google-chrome http://localhost:8666

sleep:
	sleep 1s

run: docker sleep browser

test: build
	docker-compose run --rm --entrypoint="python -m unittest" --workdir=/node/app cross discover

js: build node/web/src/node_modules node/web/src/bower_components node/web/js/app.js

node/web/src/node_modules: node/web/src/package.json
	docker-compose run --rm frontbuild npm install
	touch node/web/src/node_modules

node/web/src/bower_components: node/web/src/bower.json
	docker-compose run --rm frontbuild bower install
	touch node/web/src/bower_components

node/web/js/app.js: node/web/src/react/app.jsx
	docker-compose run --rm frontbuild gulp